//
//  CustomAlertViewDelegate.swift
//  PopUp
//
//  Created by Syncrhonous on 7/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import Foundation
//
//  CustomAlertViewDelegate.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

protocol CustomAlertViewDelegate: class {
    func okButtonTapped(selectedOption: String, textFieldValue: String)
    func cancelButtonTapped()
}
