//
//  ViewController.swift
//  PopUp
//
//  Created by Syncrhonous on 6/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var alertText: UILabel!
    @IBAction func simpleOkBtn(_ sender: UIButton) {
        displayMyAlertMsgOk(msg: "This is a simple ok alert")
    }
    
    @IBAction func simpleYesNoBtn(_ sender: UIButton) {
        displayMyAlertMsgYesNo(msg: "This is a simple yes no alert")
    }
    
    
    @IBAction func fiveSecondAlert(_ sender: UIButton) {
        visibleFiveSec()
    }
    
    @IBAction func anotherViewAlertBtn(_ sender: UIButton) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController

        customAlert.view.backgroundColor=UIColor.black.withAlphaComponent(0.6)
        self.addChild(customAlert)
        self.view.addSubview(customAlert.view)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func visibleFiveSec(){
        let alert = UIAlertController(title: "", message: "alert disappears after 5 seconds", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }


    func displayMyAlertMsgOk(msg:String){
        let myAlert=UIAlertController(title: "Attention", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction=UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.alertText.text = "ok"
        }
        
        myAlert.addAction(okAction)
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    func displayMyAlertMsgYesNo(msg:String){
        let myAlert=UIAlertController(title: "Attention", message: msg, preferredStyle: UIAlertController.Style.alert)
        
//        let yesAction=UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: nil)
//
//        let noAction=UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil)
//
        // Create the actions
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.alertText.text = "yes"
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
            self.alertText.text = "no"
        }

        myAlert.addAction(yesAction)
        myAlert.addAction(noAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }
}

