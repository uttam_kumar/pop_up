//
//  CustomAlertViewController.swift
//  PopUp
//
//  Created by Syncrhonous on 6/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
var delegate: CustomAlertViewDelegate?
class CustomAlertViewController: UIViewController {
    @IBAction func alertCancelBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //self.removeFromParent()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func cancelAlertBtnAction(_ sender: Any) {
        self.removeFromParent()
         self.dismiss(animated: true, completion: nil)
    }


}
